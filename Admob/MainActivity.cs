﻿using Android.App;
using Android.Gms.Ads;
using Android.OS;
using Android.Support.V7.App;

namespace Admob
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        InterstitialAd mInterstitialAd;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            mInterstitialAd = new InterstitialAd(this)
            {
                AdUnitId = GetString(Resource.String.interstitial_ad_unit_id),
                AdListener = new AdListener(this)
            };
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (mInterstitialAd.IsLoaded)
            {
                mInterstitialAd.Show();
            }
            else
            {
                RequestNewInterstitial();
            }
        }

        private void RequestNewInterstitial()
        {
            var adRequest = new AdRequest.Builder().Build();
            mInterstitialAd.LoadAd(adRequest);
        }

        class AdListener : Android.Gms.Ads.AdListener
        {
            MainActivity that;

            public AdListener(MainActivity t)
            {
                that = t;
            }

            public override void OnAdClosed()
            {
                that.RequestNewInterstitial();
            }
        }
    }
}